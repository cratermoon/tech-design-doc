# Tips for Design Docs and Technical Specs

A technical design doc is the third of three documents produced in a typical development process:

1. Product Requirements Document
2. Technical Requirements Document
3. Technical Design Document

- between 1 and 10 pages
- provide context

It is an aspect of software technical documentation, not the only technical document.

Include a list of people and their roles. This could include their official titles.

Explain the context: what problem are we trying to solve? Relate it to overall product, technical, and corporate strategies.

Have a "won't do" section. Be clear about what you're leaving out and why.

Discuss the existing solution (if any), how it works, and why it is how it is. Apply a variety of perspectives in describing the existing and proposed solutions. Perspectives include

- how it operates over time
- what states are possible and how to control them
- how the data are represented
- authentication and access control
- scalability
- connection: how the components communicate

In addition to describing the solution, discuss the alternatives considered and why they were rejected.

Be sure to lay out a plan for testing, monitoring, and alerting.

Discuss the impact of the solution: costs, on-call & dev-ops, support burden, system & security concerns.

Include charts and diagrams (or even videos & animations, if possible), to help explain things for different learning styles.

Development of a good technical specification involves [review](review.md) and refinement [process](process.md).

## References

- [How to write a good software design doc](https://medium.com/free-code-camp/how-to-write-a-good-software-design-document-66fcf019569c)
- [Writing Technical Design Docs](https://medium.com/machine-words/writing-technical-design-docs-71f446e42f2e)
- Beer, David F., and David A. McMurrey. 2019. *A Guide to Writing as an Engineer*.
