# Technical Spec Process

Initial pitch: Have someone more senior than you join you in a conference room and a whiteboard (or the virtual equivalent). Describe the problem, explain the proposed implementation, and try to convince them why it's the right solution. Ideally your initial reviewer would have some familiarity with the problem space. After you [complete the first draft](tips.md) of the document, get the [reviewer](review.md) to sign off. Be sure to mention their role in the final version.

Consider adding subject matter experts as early reviewers for specific aspects.

Consolidate differences and contended ideas into a Discussion section. If the discussion gets long (more than 5 comments), meet in person.

- [How to write a good software design doc](https://medium.com/free-code-camp/how-to-write-a-good-software-design-document-66fcf019569c)
- [Writing Technical Design Docs](https://medium.com/machine-words/writing-technical-design-docs-71f446e42f2e)

----
