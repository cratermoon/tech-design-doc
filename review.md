# How to Review a Technical Spec

What do you do when your role in  the [technical spec process](process.md) is reviewing one handed to you by a colleague? The [spec should answer](tips.md) the following questions:

1. What problem is it trying to solve?
2. What are the success criteria?
3. What are the most important product, business, and technical decisions and their implications?
4. What are the riskiest and most complex parts of the proposed solution?
5. What external systems does the solution interact with, and how? Does it align with long-term direction and what are the future implications?
6. What are the gaps in the design that suggest the author lacked important information?
7. What are the failure modes, and how will they be monitored and resolved?

After reading, spend five minutes thinking of the simplest alternative solution. Compare and contrast this idea with the spec and understand why/if the more complex solution is better.

## References

- [How to write a good software design doc](https://medium.com/free-code-camp/how-to-write-a-good-software-design-document-66fcf019569c)
